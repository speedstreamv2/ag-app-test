package ag.item.manager.controller;

import ag.item.manager.ApiApplication;
import ag.item.manager.config.IntegrationTestConfiguration;
import ag.item.manager.service.TaskService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.util.NestedServletException;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ApiApplication.class, IntegrationTestConfiguration.class})
@AutoConfigureMockMvc
@Slf4j
public class TaskControllerMvcTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private TaskService service;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        mvc = MockMvcBuilders.standaloneSetup(new TaskController(service)).build();
    }

    @Test
    @DisplayName("Validate success when all brackets present")
    public void validateBracketsWhenFormattedOK() throws Exception {
        mvc.perform(get("/tasks/validatebrackets?input=({})")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.input").exists())
                .andExpect(jsonPath("$.input").value("({})"))
                .andExpect(jsonPath("$.balanced").value("true"))
                .andReturn();
    }

    @Test
    @DisplayName("Validate failure when missing closing parenthesis")
    public void validateFailureWhenMissingClosingParenthesis() throws Exception {
        Assertions.assertThrows(NestedServletException.class, () -> {
            mvc.perform(get("/tasks/validatebrackets?input=({)")
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isBadRequest());
        });
    }

}
