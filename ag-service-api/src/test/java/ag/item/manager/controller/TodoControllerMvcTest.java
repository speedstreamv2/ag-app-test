package ag.item.manager.controller;

import ag.item.manager.ApiApplication;
import ag.item.manager.config.IntegrationTestConfiguration;
import ag.item.manager.model.ToDoItemAddRequest;
import ag.item.manager.service.TodoService;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ApiApplication.class, IntegrationTestConfiguration.class})
@AutoConfigureMockMvc
@Slf4j
public class TodoControllerMvcTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private TodoService service;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        mvc = MockMvcBuilders.standaloneSetup(new TodoController(service)).build();
    }

    @Test
    @DisplayName("Test successfully retrieves a ToDo Item")
    public void retrieveExistingToDoItem() throws Exception {
        mvc.perform(get("/todo/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("1"))
                .andExpect(jsonPath("$.text").value("Uulwi ifis halahs gag erh'ongg w'ssh."))
                .andExpect(jsonPath("$.createdAt").exists())
                .andExpect(jsonPath("$.completed").value("true"))
                .andReturn();
    }

    @Test
    @DisplayName("Test successfully creates a new ToDo Item")
    public void createNewToDoItem() throws Exception {
        ToDoItemAddRequest requestBody = ToDoItemAddRequest.builder()
                .text("my new todo item")
                .build();

        mvc.perform(post("/todo")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new Gson().toJson(requestBody)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("2000"))
                .andExpect(jsonPath("$.text").value("my new todo item"))
                .andExpect(jsonPath("$.createdAt").exists())
                .andExpect(jsonPath("$.completed").value("true"))
                .andReturn();
    }

    @Test
    @DisplayName("Test successfully updates an existing ToDo Item")
    public void updateExistingToDoItem() throws Exception {
        ToDoItemAddRequest requestBody = ToDoItemAddRequest.builder()
                .text("updated text for existing todo item")
                .build();

        mvc.perform(patch("/todo/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new Gson().toJson(requestBody)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("1"))
                .andExpect(jsonPath("$.text").value("updated text for existing todo item"))
                .andExpect(jsonPath("$.createdAt").exists())
                .andExpect(jsonPath("$.completed").value("true"))
                .andReturn();
    }

}
