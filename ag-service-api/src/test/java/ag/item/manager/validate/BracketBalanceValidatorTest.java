package ag.item.manager.validate;

import ag.item.manager.ApiApplication;
import ag.item.manager.exception.ToDoItemValidatorErrorException;
import ag.item.manager.config.IntegrationTestConfiguration;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ApiApplication.class, IntegrationTestConfiguration.class})
public class BracketBalanceValidatorTest {

    @Autowired
    private BracketBalanceValidator bracketBalanceValidator;

    @Test()
    @DisplayName("Test fully balanced bracket structure returns 200 OK")
    public void testFullyBalanced() throws Exception {
        String balanced = "(**, **{**, or **[**]**}**)";
        bracketBalanceValidator.validateInput(balanced);
    }

    @Test()
    @DisplayName("Test unbalanced parenthesis throws 400 Bad request with relevent message")
    public void testThrowsOnUnbalancedParenthesis() {
        String unbalanced = "(**, **{**, or **[**]***)";
        Assertions.assertThrows(ToDoItemValidatorErrorException.class, () -> {
            bracketBalanceValidator.validateInput(unbalanced);
        });
    }

    @Test()
    @DisplayName("Test unbalanced round bracket throws 400 Bad request with relevent message")
    public void testThrowsOnUnbalancedRoundBracket() {
        String unbalanced = "(**, **{**, or **[**]**}**";
        Assertions.assertThrows(ToDoItemValidatorErrorException.class, () -> {
            bracketBalanceValidator.validateInput(unbalanced);
        });
    }

}
