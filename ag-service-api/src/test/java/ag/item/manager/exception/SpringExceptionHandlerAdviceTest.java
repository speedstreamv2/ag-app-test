package ag.item.manager.exception;

import ag.item.manager.model.Detail;
import ag.item.manager.model.ToDoItemValidationError;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import java.util.ArrayList;
import java.util.List;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class SpringExceptionHandlerAdviceTest {

    @InjectMocks
    private SpringExceptionHandlerAdvice springExceptionHandlerAdvice;

    @Test
    public void testHandleToDoItemNotFoundErrorException() throws Exception {
        List<Detail> detailList = new ArrayList<>();
        detailList.add(Detail.builder()
                .location("params")
                .param("text")
                .msg(String.format("Opening char %c is unbalanced", '('))
                .value(String.format("Balance the number of occurrences of char %c with char %c", '(', ')'))
                .build());

        Detail detailArray[] = new Detail[detailList.size()];
        detailArray = detailList.toArray(detailArray);
        ToDoItemValidationError toDoItemValidationError = ToDoItemValidationError.builder()
                .details(detailArray)
                .name("ValidationError")
                .build();

        ToDoItemValidatorErrorException exception = new ToDoItemValidatorErrorException(toDoItemValidationError);
        ResponseEntity responseEntity = springExceptionHandlerAdvice.handleToDoItemValidationErrorBadRequestException(exception);

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        ToDoItemValidationError responseValidationError = (ToDoItemValidationError) responseEntity.getBody();
        Detail[] detailArr = responseValidationError.getDetails();
        assertThat(responseValidationError.getName()).isEqualTo("ValidationError");
        assertThat(detailArr[0].getLocation()).isEqualTo("params");
        assertThat(detailArr[0].getParam()).isEqualTo("text");
        assertThat(detailArr[0].getMsg()).isEqualTo("Opening char ( is unbalanced");
        assertThat(detailArr[0].getValue()).isEqualTo("Balance the number of occurrences of char ( with char )");
    }

}
