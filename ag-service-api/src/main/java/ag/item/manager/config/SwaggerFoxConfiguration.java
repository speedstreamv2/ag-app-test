package ag.item.manager.config;

import com.google.common.base.Predicate;
import com.google.common.collect.Sets;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static com.google.common.base.Predicates.or;
import static springfox.documentation.builders.PathSelectors.regex;

@Configuration
@EnableSwagger2
public class SwaggerFoxConfiguration extends WebMvcConfigurationSupport {

    public static final String ROOT = "/";

    @Bean
    public Docket updateApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .forCodeGeneration(true)
                .protocols(Sets.newHashSet("https"))
                .apiInfo(getApiInfo())
                .enableUrlTemplating(true)
                .pathMapping(ROOT)
                .select()
                .paths((com.google.common.base.Predicate<String>) includedPaths())
                .build();
    }

    private Predicate<String> includedPaths() {
        return or(
                regex("/status.*"),
                regex("/tasks/validatebrackets.*"),
                regex("/todo.*")
        );
    }

    private ApiInfo getApiInfo() {
        return new ApiInfoBuilder()
                .title("Auto General Test APP Api Service")
                .description("Auto General Test APP Api Services provide the ability to perform tasks, miscellaneous and todo functions. ")
                .version("1.0")
                .contact(new Contact("Customer Support", "https://foo.general", "api.support@foo.general"))
                .build();
    }

    @Override
    protected void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
    }

}
