package ag.item.manager.validate;

import ag.item.manager.exception.ToDoItemValidatorErrorException;
import ag.item.manager.model.Detail;
import ag.item.manager.model.ToDoItemValidationError;
import ag.item.manager.util.ItemUtil;
import org.springframework.stereotype.Controller;

@Controller
public final class BracketBalanceValidator {

    private static final char OPENING_ROUND_BRACKET = '(';
    private static final char CLOSING_ROUND_BRACKET = ')';

    private static final char OPENING_PARENTHESIS = '{';
    private static final char CLOSING_PARENTHESIS = '}';

    private static final char OPENING_SQUARE_BRACKET = '(';
    private static final char CLOSING_SQUARE_BRACKET = ')';

    public void validateInput(String input) {
        validateBracket(input, OPENING_ROUND_BRACKET, CLOSING_ROUND_BRACKET);
        validateBracket(input, OPENING_PARENTHESIS, CLOSING_PARENTHESIS);
        validateBracket(input, OPENING_SQUARE_BRACKET, CLOSING_SQUARE_BRACKET);
    }

    private void validateBracket(String input, char openingChar, char closingChar) {
        int openingBracketCount = 0;
        for (int i = 0; i < input.length(); i++) {
            if (input.charAt(i) == openingChar) {
                openingBracketCount++;
            }
        }

        int closingBracketCount = 0;
        for (int i = 0; i < input.length(); i++) {
            if (input.charAt(i) == closingChar) {
                closingBracketCount++;
            }
        }

        if (openingBracketCount != closingBracketCount) {
            Detail[] detailArray = ItemUtil.createErrorDetailsArray("params", "text",
                    String.format("Opening character %c is unbalanced", openingChar),
                    String.format("Balance the number of occurrences of opening character %c with closing character %c", openingChar, closingChar));
            ToDoItemValidationError toDoItemValidationError = ToDoItemValidationError.builder()
                    .details(detailArray)
                    .name("ValidationError")
                    .build();
            throw new ToDoItemValidatorErrorException(toDoItemValidationError);
        }
    }

}

