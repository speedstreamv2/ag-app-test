package ag.item.manager.controller;

import ag.item.manager.constants.SwaggerConstants;
import ag.item.manager.model.BalanceTestResult;
import ag.item.manager.service.TaskService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@AllArgsConstructor
public class TaskController {

    @Autowired
    private TaskService taskService;

    @ApiOperation(notes = SwaggerConstants.GET_TASKS_API_OPERATION_DESCRIPTION, nickname = SwaggerConstants.GET_TASKS_API_OPERATION_NICKNAME, value = SwaggerConstants.GET_TASKS_API_OPERATION_VALUE)
    @ApiImplicitParams(value = {@ApiImplicitParam(name = "input", value = SwaggerConstants.GET_TASKS_BRACKETS_IN_A_STRING_PARAM_DESC, paramType = "query", dataType = "string", required = true)})
    @RequestMapping(value = "/tasks/validatebrackets", method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE)
    public BalanceTestResult validateBrackets(@RequestParam String input) {
        return taskService.validateBrackets(input);
    }

}
