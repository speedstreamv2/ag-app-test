package ag.item.manager.controller;

import ag.item.manager.constants.SwaggerConstants;
import ag.item.manager.model.ToDoItem;
import ag.item.manager.model.ToDoItemAddRequest;
import ag.item.manager.service.TodoService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@AllArgsConstructor
public class TodoController {

    @Autowired
    private TodoService todoService;

    @ApiOperation(notes = SwaggerConstants.POST_TODO_API_OPERATION_DESCRIPTION, nickname = SwaggerConstants.POST_TODO_API_OPERATION_NICKNAME, value = SwaggerConstants.POST_TODO_API_OPERATION_VALUE)
    @RequestMapping(value = "/todo", method = RequestMethod.POST, produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    public ToDoItem createToDoItem(@RequestBody ToDoItemAddRequest body) {
        return todoService.createTodoItem(body);
    }


    @ApiOperation(notes = SwaggerConstants.GET_TODO_ITEM_API_OPERATION_DESCRIPTION, nickname = SwaggerConstants.GET_TODO_ITEM_API_OPERATION_NICKNAME, value = SwaggerConstants.GET_TODO_ITEM_API_OPERATION_VALUE)
    @ApiImplicitParams(value = {@ApiImplicitParam(name = "id", value = SwaggerConstants.TODO_ID_IN_AN_INT_PARAM_DESC, paramType = "path", dataType = "int", required = true)})
    @RequestMapping(value = "/todo/{id}", method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE)
    public ToDoItem getTodoItem(@PathVariable int id) {
        return todoService.getTodoItem(id);
    }


    @ApiOperation(notes = SwaggerConstants.PATCH_TODO_ITEM_API_OPERATION_DESCRIPTION, nickname = SwaggerConstants.PATCH_TODO_ITEM_API_OPERATION_NICKNAME, value = SwaggerConstants.PATCH_TODO_ITEM_API_OPERATION_VALUE)
    @ApiImplicitParams(value = {@ApiImplicitParam(name = "id", value = SwaggerConstants.TODO_ID_IN_AN_INT_PARAM_DESC, paramType = "path", dataType = "int", required = true)})
    @RequestMapping(value = "/todo/{id}", method = RequestMethod.PATCH, produces = APPLICATION_JSON_VALUE)
    public ToDoItem updateTodoItem(@PathVariable int id, @RequestBody ToDoItemAddRequest body) {
        return todoService.updateTodoItem(id, body);
    }

}
