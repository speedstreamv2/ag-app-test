package ag.item.manager.controller;

import ag.item.manager.constants.SwaggerConstants;
import ag.item.manager.model.StatusResponse;
import ag.item.manager.service.MiscService;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@AllArgsConstructor
public class MiscController {

    @Autowired
    private MiscService miscService;

    @ApiOperation(notes = SwaggerConstants.GET_MISC_API_OPERATION_DESCRIPTION, nickname = SwaggerConstants.GET_MISC_API_OPERATION_NICKNAME, value = SwaggerConstants.GET_MISC_API_OPERATION_VALUE)
    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE)
    public StatusResponse getSystemStatus() {
        return miscService.getSystemStatus();
    }

}
