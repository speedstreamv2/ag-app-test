package ag.item.manager.service;

import ag.item.manager.exception.ToDoItemNotFoundException;
import ag.item.manager.exception.ToDoItemValidatorErrorException;
import ag.item.manager.model.ToDoItem;
import ag.item.manager.model.Detail;
import ag.item.manager.model.ToDoItemAddRequest;
import ag.item.manager.model.ToDoItemNotFoundError;
import ag.item.manager.model.ToDoItemValidationError;
import ag.item.manager.util.ItemUtil;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;

@Service
@AllArgsConstructor
public class TodoService {

    public ToDoItem createTodoItem(ToDoItemAddRequest body) {
        validateBody(body);
        return ToDoItem.builder()
                .id(2000)
                .createdAt(ZonedDateTime.now())
                .isCompleted(true)
                .text(body.getText())
                .build();
    }

    public ToDoItem getTodoItem(int id) {
        validateId(id);
        return ToDoItem.builder()
                .id(id)
                .createdAt(ZonedDateTime.now())
                .isCompleted(true)
                .text("Uulwi ifis halahs gag erh'ongg w'ssh.")
                .build();
    }

    public ToDoItem updateTodoItem(int id, ToDoItemAddRequest body) {
        validateId(id);
        validateBody(body);
        return ToDoItem.builder()
                .id(id)
                .createdAt(ZonedDateTime.now())
                .isCompleted(true)
                .text(body.getText())
                .build();
    }

    private void validateId(int id) {
        // Simulate validation error when id = 1000, as an id that doesn't exist
        if (id == 1000) {
            Detail[] detailArray = ItemUtil.createErrorDetailsArray("params", "id", "Invalid id, not found", "id validation");
            throw new ToDoItemNotFoundException(ToDoItemNotFoundError.builder()
                    .details(detailArray)
                    .name("NotFoundError")
                    .build());
        }

        // Simulate validation error when id = 0, as an id must be greater than zero
        if (id == 0) {
            Detail[] detailArray = ItemUtil.createErrorDetailsArray("params", "id", "Must be greater than 0", "id validation");
            throw new ToDoItemValidatorErrorException(ToDoItemValidationError.builder()
                    .details(detailArray)
                    .name("ValidationError")
                    .build());
        }
    }

    private void validateBody(ToDoItemAddRequest body) {
        if (StringUtils.isEmpty(body.getText())) {
            Detail[] detailArray = ItemUtil.createErrorDetailsArray("params", "text", "Must be between 1 and 50 chars long", "text body validation");
            throw new ToDoItemValidatorErrorException(ToDoItemValidationError.builder()
                    .details(detailArray)
                    .name("ValidationError")
                    .build());
        }
    }

}
