package ag.item.manager.service;

import ag.item.manager.model.StatusEnum;
import ag.item.manager.model.StatusResponse;
import org.springframework.stereotype.Service;

@Service
public class MiscService {

    public StatusResponse getSystemStatus() {
        // Hardcoded, but the value would normally be derived by pinging the backend for a heart beat
        return StatusResponse.builder()
                .status(StatusEnum.HEALTHY.getStatusValue())
                .build();
    }

}
