package ag.item.manager.service;

import ag.item.manager.validate.BracketBalanceValidator;
import ag.item.manager.model.BalanceTestResult;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class TaskService {

    @Autowired
    private BracketBalanceValidator bracketBalanceValidator;

    public BalanceTestResult validateBrackets(String input) {

        bracketBalanceValidator.validateInput(input);

        return BalanceTestResult.builder()
                .input(input)
                .isBalanced(true)
                .build();
    }

}
