package ag.item.manager.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ToDoItemAddRequest {

    @ApiModelProperty(example = "Uulwi ifis halahs gag erh'ongg w'ssh.")
    @Min(value = 1, message = "Length must be a minimum of 1")
    @Max(value = 50, message = "Length must be a maximum of 50")
    private String text;

}
