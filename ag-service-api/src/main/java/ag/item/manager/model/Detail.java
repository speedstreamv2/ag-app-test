package ag.item.manager.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Detail {

    private String location;
    private String param;
    private String msg;
    private String value;

}
