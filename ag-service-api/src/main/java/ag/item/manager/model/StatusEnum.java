package ag.item.manager.model;

public enum StatusEnum {

    HEALTHY("healthy"),
    RESTARTING("restarting"),
    DOWN("down");

    private String statusValue;

    StatusEnum(String statusValue) {
        this.statusValue = statusValue;
    }

    public String getStatusValue() {
        return statusValue;
    }

}
