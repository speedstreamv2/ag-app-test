package ag.item.manager.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class StatusResponse {

    @ApiModelProperty(example = "healthy")
    private String status;

}
