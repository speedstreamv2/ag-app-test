package ag.item.manager.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import java.time.ZonedDateTime;

@Data
@Builder
public class ToDoItem {

    @ApiModelProperty(example = "42")
    private int id;

    @ApiModelProperty(example = "Uulwi ifis halahs gag erh'ongg w'ssh.")
    private String text;

    @ApiModelProperty(example = "false")
    private boolean isCompleted;

    @ApiModelProperty(example = "2017-10-13T01:50:58.735Z")
    private ZonedDateTime createdAt;

}
