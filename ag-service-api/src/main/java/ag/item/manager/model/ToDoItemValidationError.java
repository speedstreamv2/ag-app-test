package ag.item.manager.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ToDoItemValidationError {

    private Detail[] details;

    @ApiModelProperty(example = "ValidationError")
    private String name;
}
