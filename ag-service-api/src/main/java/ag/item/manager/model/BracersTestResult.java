package ag.item.manager.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class BracersTestResult {

    private String input;
    private String result;
    private String expected;
    private String isCorrect;

}
