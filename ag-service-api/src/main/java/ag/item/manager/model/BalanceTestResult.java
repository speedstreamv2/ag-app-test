package ag.item.manager.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class BalanceTestResult {

    @ApiModelProperty(value = "input", example = "[(]")
    private String input;

    @ApiModelProperty(value = "input", example = "false")
    private boolean isBalanced;
}
