package ag.item.manager.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ToDoTestResult {

    private String input;
    private ToDoItem result;

}
