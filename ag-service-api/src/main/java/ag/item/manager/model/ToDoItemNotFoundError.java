package ag.item.manager.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ToDoItemNotFoundError {

    private Detail[] details;

    @ApiModelProperty(example = "NotFoundError")
    private String name;

}
