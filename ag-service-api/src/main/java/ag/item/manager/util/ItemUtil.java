package ag.item.manager.util;

import ag.item.manager.model.Detail;

import java.util.ArrayList;
import java.util.List;

public final class ItemUtil {

    private ItemUtil() {
    }

    public static Detail[] createErrorDetailsArray(String location, String param, String msg, String value) {
        List<Detail> detailList = new ArrayList<>();
        detailList.add(Detail.builder()
                .location(location)
                .param(param)
                .msg(msg)
                .value(value)
                .build());

        Detail detailArray[] = new Detail[detailList.size()];
        return detailList.toArray(detailArray);
    }

}
