package ag.item.manager.exception;

import ag.item.manager.model.ToDoItemNotFoundError;
import ag.item.manager.model.ToDoItemValidationError;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class SpringExceptionHandlerAdvice extends ResponseEntityExceptionHandler {

    @ExceptionHandler(ToDoItemValidatorErrorException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ResponseEntity<ToDoItemValidationError> handleToDoItemValidationErrorBadRequestException(final ToDoItemValidatorErrorException e) {
        return new ResponseEntity<>(e.getToDoItemValidationError(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ToDoItemNotFoundException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ResponseEntity<ToDoItemNotFoundError> handleToDoItemNotFoundErrorBadRequestException(final ToDoItemNotFoundException e) {
        return new ResponseEntity<>(e.getToDoItemNotFoundError(), HttpStatus.BAD_REQUEST);
    }

}
