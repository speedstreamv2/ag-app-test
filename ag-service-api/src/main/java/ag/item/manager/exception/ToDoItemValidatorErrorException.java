package ag.item.manager.exception;

import ag.item.manager.model.ToDoItemValidationError;
import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class ToDoItemValidatorErrorException extends RuntimeException {

    private ToDoItemValidationError toDoItemValidationError;

}
