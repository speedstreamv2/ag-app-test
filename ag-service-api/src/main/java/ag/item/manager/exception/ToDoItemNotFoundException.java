package ag.item.manager.exception;

import ag.item.manager.model.ToDoItemNotFoundError;
import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class ToDoItemNotFoundException extends RuntimeException {

    private ToDoItemNotFoundError toDoItemNotFoundError;

}
