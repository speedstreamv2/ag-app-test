package ag.item.manager.constants;

public final class SwaggerConstants {

    private SwaggerConstants() {
    }

    public static final String GET_TASKS_API_OPERATION_DESCRIPTION = "Checks if brackets in a string are balanced";
    public static final String GET_TASKS_API_OPERATION_NICKNAME = "Checks brackets in string";
    public static final String GET_TASKS_API_OPERATION_VALUE = "Checks brackets in string";

    public static final String GET_TASKS_BRACKETS_IN_A_STRING_PARAM_DESC = "Brackets in a string are considered to be balanced if the following criteria are met." +
            "For every opening bracket (i.e., (, {, or [), there is a matching closing bracket (i.e., ), }, or ]) of the same type (i.e., ( matches ), { matches }, and [ matches ]). An opening bracket must appear before (to the left of) its matching closing bracket. For example, ]{}[ is not balanced.\n" +
            "No unmatched braces lie between some pair of matched bracket. For example, ({[]}) is balanced, but {[}] and [{)] are not balanced.";

    public static final String POST_TODO_API_OPERATION_DESCRIPTION = "Create a todo item";
    public static final String POST_TODO_API_OPERATION_NICKNAME = "Create a todo item";
    public static final String POST_TODO_API_OPERATION_VALUE = "Create a todo item";

    public static final String GET_TODO_ITEM_API_OPERATION_DESCRIPTION = "Retrieve a specific item by id";
    public static final String GET_TODO_ITEM_API_OPERATION_NICKNAME = "Retrieve a todo item by id";
    public static final String GET_TODO_ITEM_API_OPERATION_VALUE = "Retrieve a todo item by id";

    public static final String TODO_ID_IN_AN_INT_PARAM_DESC = "Retrieves a specific item by id.";

    public static final String PATCH_TODO_ITEM_API_OPERATION_DESCRIPTION = "Modify an item";
    public static final String PATCH_TODO_ITEM_API_OPERATION_NICKNAME = "Modify item";
    public static final String PATCH_TODO_ITEM_API_OPERATION_VALUE = "Modify item";

    public static final String GET_MISC_API_OPERATION_DESCRIPTION = "Returns system's status";
    public static final String GET_MISC_API_OPERATION_NICKNAME = "Returns status";
    public static final String GET_MISC_API_OPERATION_VALUE = "Returns status";

}
