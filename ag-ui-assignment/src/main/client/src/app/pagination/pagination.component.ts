import {Component, Input} from '@angular/core';
import {PaginationDTO} from "./PaginationDTO";

export class PaginationComponent {
  public pagesArray: Array<number> = [];
  public currentPage: number = 1;

  @Input() set setPagination(pagination: PaginationDTO) {
    if (pagination) {
      const pagesAmount = Math.ceil(
        pagination.itemsCount / pagination.pageSize
      );
      this.pagesArray = new Array(pagesAmount).fill(1);
    }
  }
}

