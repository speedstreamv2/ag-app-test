export interface PaginationDTO {
  itemsCount: number;
  pageSize: number;
}
