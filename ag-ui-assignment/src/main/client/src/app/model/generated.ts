/* tslint:disable */
/* eslint-disable */

export class UserDTO {
    id: number;
    username: string;
    about: string;
    karma: string;
    created_at: Date;
    avg: number;
    delay: number;
    submitted: number;
    updated_at: Date;
    submission_count: number;
    comment_count: number;
    created_at_i: Date;
    objectID: string;
}
