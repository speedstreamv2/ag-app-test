export class AppConstants {
  public static APP_TITLE = "UI-ASSIGNMENT";
  public static APP_CONTEXT = "/uiassignment";

  // ---- Router path matches
  // Shared Router paths matches
  public static ROUTER_USER = "user";
  public static ROUTER_ITEM = "item";

}
