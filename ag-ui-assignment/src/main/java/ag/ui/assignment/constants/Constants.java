package ag.ui.assignment.constants;

public class Constants {

    private Constants() {
    }

    public static final String BASE_REST_API = "/test-ui-assignment";
    public static final String BASE_ALGOLIA_REST_API = "http://hn.algolia.com/api/v1";

}
