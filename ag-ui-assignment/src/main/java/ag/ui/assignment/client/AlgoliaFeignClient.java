package ag.ui.assignment.client;

import ag.ui.assignment.constants.Constants;
import ag.ui.assignment.model.UserDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@FeignClient(name="algolia", path = Constants.BASE_ALGOLIA_REST_API ) // Service Id of Algolia API service
public interface AlgoliaFeignClient {

    @GetMapping(path = "/users/{username}", consumes = APPLICATION_JSON_VALUE)
    UserDTO getUser(@RequestParam("username") String username);

}
