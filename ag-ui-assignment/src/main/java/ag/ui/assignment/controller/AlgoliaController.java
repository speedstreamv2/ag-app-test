package ag.ui.assignment.controller;

import ag.ui.assignment.client.AlgoliaFeignClient;
import ag.ui.assignment.model.UserDTO;
import ag.ui.assignment.constants.Constants;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@Slf4j
@AllArgsConstructor
@RequestMapping(Constants.BASE_REST_API)
public class AlgoliaController {

    @Autowired
    private AlgoliaFeignClient algoliaFeignClient;

    @RequestMapping(value = "/users/{username}", method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE)
    public UserDTO getUser(@PathVariable String username) {
        return algoliaFeignClient.getUser(username);
    }

}
