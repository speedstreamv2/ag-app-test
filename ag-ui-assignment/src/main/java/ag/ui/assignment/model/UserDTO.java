package ag.ui.assignment.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.time.ZonedDateTime;

@Data
@Builder
public class UserDTO {

    @JsonProperty("id")
    private int id;

    @JsonProperty("username")
    private String username;

    @JsonProperty("about")
    private String about;

    @JsonProperty("karma")
    private String karma;

    @JsonProperty("created_at")
    private ZonedDateTime created_at;

    @JsonProperty("avg")
    private Double avg;

    @JsonProperty("delay")
    private Double delay;

    @JsonProperty("submitted")
    private int submitted;

    @JsonProperty("updated_at")
    private ZonedDateTime updated_at;

    @JsonProperty("submission_count")
    private int submission_count;

    @JsonProperty("comment_count")
    private int comment_count;

    @JsonProperty("created_at_i")
    private ZonedDateTime created_at_i;

    @JsonProperty("objectID")
    private String objectID;

}
