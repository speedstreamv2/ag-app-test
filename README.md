# auto-and-general-test

Import the base pom from the project, which in turn will import 3 maven projects.
* ag-service-api (containing a spring boot 2.2 Service API with three RestControllers with various endpoints)
* ag-ui (containing a spring boot 2.2 app with Angular typescript UI for the backend service, partially completed)
* ag-ui-assignment (containing a spring boot 2.2 app with Angular typescript UI which integrates with the existing Algolia Rest API)

Building the project:

BACK END Assignment
=======
From this location: ag-app-test/ag-service-api
* mvn clean install
* mvn spring-boot: run

This will run up the Service API on:
* context-path: /test-service
* port: 8081
  
Use this Postman collection to test the various endpoints with various scenarios.
* Ag-App-Test.postman_collection.json 

FRONT END (UI for the ag-service-api)
=============  
From this location: ag-app-test/ag-ui
* mvn clean install
* mvn spring-boot: run

This will run up the Frontend APP on:
* context-path: /test-ui
* port: 8082


FRONT END (UI Assignment - separate backend)
=============  
From this location: ag-app-test/ag-ui-assignment
* mvn clean install
* mvn spring-boot: run

This will run up the Frontend UI Assignment APP on:
* context-path: /test-ui-assignment
* port: 8083

ng serve will run up the Angular application 
* http://localhost:4200/user (load the user) - incomplete, needed to hook into the Rest API and display
* http://localhost:4200/item (load all the items) - incomplete, needed to hook into the Rest API and display


