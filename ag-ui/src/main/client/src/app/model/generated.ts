/* tslint:disable */
/* eslint-disable */

export class BalanceTestResult {
    input: string;
    balanced: boolean;
}

export class BracersTestResult {
    input: string;
    result: string;
    expected: string;
    isCorrect: string;
}

export class Detail {
    location: string;
    param: string;
    msg: string;
    value: string;
}

export class StatusResponse {
    status: string;
}

export class ToDoItem {
    id: number;
    text: string;
    createdAt: Date;
    completed: boolean;
}

export class ToDoItemAddRequest {
    text: string;
}

export class ToDoItemNotFoundError {
    details: Detail[];
    name: string;
}

export class ToDoItemUpdateRequest {
    text: string;
    completed: boolean;
}

export class ToDoItemValidationError {
    details: Detail[];
    name: string;
}

export class ToDoTestResult {
    input: string;
    result: ToDoItem;
}

export const enum StatusEnum {
    HEALTHY = "HEALTHY",
    RESTARTING = "RESTARTING",
    DOWN = "DOWN",
}
