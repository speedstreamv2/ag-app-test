package ag.ui.controller;

import ag.item.manager.model.BalanceTestResult;
import ag.ui.client.TaskFeignClient;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@Slf4j
@AllArgsConstructor
public class TaskController {

    @Autowired
    private TaskFeignClient taskFeignClient;

    @RequestMapping(value = "/tasks/validatebrackets", method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE)
    public BalanceTestResult validateBrackets(@RequestParam String input) {
        //return taskFeignClient.validateBrackets(input);
        return null;
    }

}
