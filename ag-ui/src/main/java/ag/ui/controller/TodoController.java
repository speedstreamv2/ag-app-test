package ag.ui.controller;

import ag.item.manager.model.ToDoItem;
import ag.item.manager.model.ToDoItemAddRequest;
import ag.ui.client.TodoFeignClient;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@Slf4j
@AllArgsConstructor
public class TodoController {

    @Autowired
    private TodoFeignClient todoFeignClient;

    @RequestMapping(value = "/todo", method = RequestMethod.POST, produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    public ToDoItem createToDoItem(@RequestBody ToDoItemAddRequest body) {
        return todoFeignClient.createTodoItem(body);
    }

    @RequestMapping(value = "/todo/{id}", method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE)
    public ToDoItem getTodoItem(@PathVariable int id) {
        return todoFeignClient.getTodoItem(id);
    }

    @RequestMapping(value = "/todo/{id}", method = RequestMethod.PATCH, produces = APPLICATION_JSON_VALUE)
    public ToDoItem updateTodoItem(@PathVariable int id, @RequestBody ToDoItemAddRequest body) {
        return todoFeignClient.updateTodoItem(id, body);
    }

}
