package ag.ui.controller;

import ag.item.manager.model.StatusResponse;
import ag.ui.client.MiscFeignClient;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@Slf4j
@AllArgsConstructor
public class MiscController {

    @Autowired
    private MiscFeignClient miscFeignClient;

    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE)
    public StatusResponse getSystemStatus() {
        return miscFeignClient.getSystemStatus();
    }

}
