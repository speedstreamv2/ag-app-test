package ag.ui.client;

import ag.item.manager.model.StatusResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@FeignClient(name="misc" ) // Service Id of Misc API service
public interface MiscFeignClient {

    @GetMapping(path = "/status", produces = APPLICATION_JSON_VALUE)
    StatusResponse getSystemStatus();

}
