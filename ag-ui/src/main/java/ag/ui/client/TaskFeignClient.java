package ag.ui.client;

import ag.item.manager.model.ToDoItem;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@FeignClient(name="task" ) // Service Id of Task API service
public interface TaskFeignClient {

    @GetMapping(path = "/tasks/validatebrackets", consumes = APPLICATION_JSON_VALUE)
    ToDoItem getTodoItem(@RequestParam("input") String input);

}
