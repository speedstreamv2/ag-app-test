package ag.ui.constants;

public final class Constants {

    private Constants() {
    }

    public static final String BASE_REST_API = "/test-service";

}
