package ag.ui;

import org.springframework.boot.Banner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableDiscoveryClient
@EnableFeignClients
@SpringBootApplication(exclude = SecurityAutoConfiguration.class)
public class UiApplication {

    public static void main(String[] args) {
        new SpringApplicationBuilder(UiApplication.class)
                .bannerMode(Banner.Mode.OFF)
                .run(args);
    }

}
